import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  get token(): string | null {
    const expDate = new Date(sessionStorage.getItem('token-exp'));
    if (new Date() > expDate) {
      this.logout();
      return null;
    }
    return sessionStorage.getItem('token');
  }

  // todo delete all ANY type
  login(user): Observable<any> {
    return this.http.post('https://packs.polito.uz/api/v1/dicom/auth/login/', user)
      .pipe(tap(this.setToken));
  }

  logout(): void {
    this.setToken(null);
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  setToken(response): void {
    if (response) {
      const expDate = new Date(new Date().getTime() + 3600 * 1000);
      sessionStorage.setItem('token', response.token);
      sessionStorage.setItem('token-exp', expDate.toString());
    } else {
      sessionStorage.clear();
    }
  }
}
