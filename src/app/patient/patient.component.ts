import {Component, OnInit} from '@angular/core';
import {StudyService} from '../shared/services/study.service';

import Hammer from 'hammerjs/hammer.min';
import * as cornerstoneMath from 'cornerstone-math/dist/cornerstoneMath.min';
import * as dicomParser from 'dicom-parser/dist/dicomParser.min';
import * as cornerstone from 'cornerstone-core/dist/cornerstone.min';
import * as cornerstoneTools from 'cornerstone-tools/dist/cornerstoneTools.min';
import * as cornerstoneWADOImageLoader from 'cornerstone-wado-image-loader/dist/cornerstoneWADOImageLoader.min';

import {ActivatedRoute} from '@angular/router';
import {FileSet} from '../shared/interfaces';
import {AlertService} from '../shared/services/alert.service';

import {DocumentCreator} from './doc-generator';
import {achievements, diagnostics, experiences, skills} from './doc-data';
import {Packer} from 'docx';
import {saveAs} from 'file-saver/dist/FileSaver.min';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {
  baseUrl = 'https://packs.polito.uz';
  patientId = null;
  patientName = '';

  imageId = [];
  seriesSet = [];
  fileSet: FileSet[] = [];
  sidebarImg: [];
  seriesSetFiles = [];
  seriesSetNumber = 0;
  dcmImg = 1;
  row = 1;
  col = 1;
  countGrid = [];
  sideBarIndex = 0;
  toolData: any;
  toolDataList = [];
  setData: any[] = [];

  lengthData = [];
  ellipseData = [];
  arrowData = [];
  angleData = [];
  probeData = [];
  rectangleData = [];
  bidirectionalData = [];


  // toolDataLength = 0;

  loading = false;

  isSeriesActive = false;
  isMesActive = false;
  isMoreOpen = false;
  isMenuOpen = false;
  isLayoutOpen = false;
  isHover = [false, false, false, false, false, false, false, false, false];
  isScrollActive = false;
  isHidden = true;

  activeToolName = '';
  toolName = 'StackScrollMouseWheel';

  constructor(private studyService: StudyService,
              private route: ActivatedRoute,
              private alert: AlertService) {
  }

  ngOnInit(): void {
    document.oncontextmenu = document.body.oncontextmenu = () => {
      return false;
    };
    cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
    cornerstoneWADOImageLoader.external.dicomParser = dicomParser;
    cornerstoneTools.external.cornerstoneMath = cornerstoneMath;
    cornerstoneTools.external.cornerstone = cornerstone;
    cornerstoneTools.external.Hammer = Hammer;

    this.route.params.subscribe(res => {
      this.patientId = res.id;
    });

    this.loading = true;

    // reload elements
    this.studyService.getStudyById(this.patientId).subscribe(next => {
      if (next.firstname && next.lastname) {
        this.patientName = next.firstname + ' ' + next.lastname
      } else {
        this.patientName = next.patient_name;
      }
      this.seriesSet = next.study_set[0].series_set;

      this.seriesSet.map(res => {
        if (res.file_set.length) {
          this.seriesSetFiles.push('https://packs.polito.uz/' + res.file_set[0].image);
        }
      });

      this.fileSet = next.study_set[0].series_set[this.seriesSetNumber].file_set;
      this.sidebarImg = next.study_set[0].series_set[0].file_set[0].image;
      console.log('this.fileSet', this.fileSet);
      this.fileSet.forEach((file: any) => {
        this.imageId.push('wadouri:' + this.baseUrl + file.file);
      });

      this.loadAllElements(this.dcmImg);

    }, () => {

    }, () => {
      this.levelsTool('');
      this.stackScrollTool('');
      this.loading = false;
    });
  }

  // loadAllElements
  loadAllElements(i: number): void {
    const element = document.getElementById(`dcm-${i}`);

    cornerstoneTools.init({
      showSVGCursors: true,
      globalToolSyncEnabled: true,
      mouseEnabled: true,
      touchEnabled: true,
      autoResizeViewports: true
    });

    cornerstone.enable(element);

    const stack = {
      currentImageIdIndex: 0,
      imageIds: this.imageId,
    };

    element.tabIndex = 0;
    element.focus();

    cornerstone.loadAndCacheImage(this.imageId[0]).then((image) => {
      cornerstone.displayImage(element, image);
      cornerstoneTools.addStackStateManager(element, ['stack']);
      cornerstoneTools.addToolState(element, 'stack', stack);
    });

  }

  loadFiles(i: number): void {
    this.sideBarIndex = i;
    this.imageId = [];

    this.fileSet = this.seriesSet[i].file_set;

    this.fileSet.forEach((post: any) => {
      this.imageId.push('wadouri:' + this.baseUrl + post.file);
    });

    this.loadAllElements(this.dcmImg);
  }

  checkToolState(): void {
    const toolState = cornerstoneTools.globalImageIdSpecificToolStateManager.saveToolState();

    this.toolData = Object.entries(toolState);

    this.toolDataList = [];

    for (let i = 0; i < this.toolData.length; i++) {
      if (this.toolData[i][1].Length) {
        this.toolData[i][1].Length.data.forEach(item => {
          this.toolDataList.push({name: 'Length', data: item.length.toFixed(2)});
        });
      }
      if (this.toolData[i][1].ArrowAnnotate) {
        this.toolData[i][1].ArrowAnnotate.data.forEach(item => {
          this.toolDataList.push({name: 'Annotate', data: item.text});
        });
      }
      if (this.toolData[i][1].Angle) {
        this.toolData[i][1].Angle.data.forEach(item => {
          this.toolDataList.push({name: 'Angle', data: item.rAngle.toFixed(2)});
        });
      }
      if (this.toolData[i][1].Probe) {
        this.toolData[i][1].Probe.data.forEach(item => {
          this.toolDataList.push({
            name: 'Probe',
            data: {x: item.cachedStats.x.toFixed(2), y: item.cachedStats.y.toFixed(2)}
          });
        });
      }
      if (this.toolData[i][1].EllipticalRoi) {
        this.toolData[i][1].EllipticalRoi.data.forEach(item => {
          this.toolDataList.push({name: 'Elliptical', data: item.cachedStats.area.toFixed(2)});
        });
      }
      if (this.toolData[i][1].RectangleRoi) {
        this.toolData[i][1].RectangleRoi.data.forEach(item => {
          this.toolDataList.push({name: 'Rectangle', data: item.cachedStats.area.toFixed(2)});
        });

      }
      if (this.toolData[i][1].Bidirectional) {
        this.toolData[i][1].Bidirectional.data.forEach(item => {
          this.toolDataList.push({
            name: 'Bidirectional',
            data: {
              longestDiameter: item.longestDiameter,
              shortestDiameter: item.shortestDiameter
            }
          });
        });
      }
    }
  }

  saveData(): void {
    const toolState = cornerstoneTools.globalImageIdSpecificToolStateManager.saveToolState();
    const fileData = [];
    const saveData = [];

    const results = this.fileSet
      .filter(({file: file}) => Object.entries(toolState)
        .some(({0: id}) => id.split('/')
          .pop() === file.split('/').pop())
      );
    results.forEach(item => fileData.push({id: item.id, file: item.file}));

    for (let i = 0; i < fileData.length; i++) {
      for (let j = 0; j < Object.entries(toolState).length; j++) {
        if (fileData[i].file.split('/').pop() === Object.entries(toolState)[j][0].split('/').pop()) {
          saveData.push({id: fileData[i].id, data: Object.entries(toolState)[j][1]});
        }
      }
    }

    {
      if (Object.entries(toolState).length) {
        for (let i = 0; i < saveData.length; i++) {
          this.studyService.saveData({file_id: saveData[i].id, data: saveData[i].data}).subscribe(next => {
            this.alert.success(next.message);
            if (next.message === 'Data is created') {
              this.alert.success(next.message);
            } else if (next.message === 'Data is updated') {
              this.alert.warning(next.message);
            }
          });
        }
      }
    }
  }

  getData(): void {
    const drawData = [];

    this.studyService.getData().subscribe(next => {
      this.setData = next;
      this.setData.forEach(item => {
        this.fileSet.forEach(item1 => {
          if (item.file === item1.id) {
            drawData.push({imageId: 'wadouri:https://packs.polito.uz' + item1.file, data: item.data});
          }
        });
      });
    }, (err) => {
      console.error(err);
    }, () => {
      drawData.forEach(item => {
        cornerstoneTools.globalImageIdSpecificToolStateManager.restoreImageIdToolState(item.imageId, item.data);
        cornerstone.updateImage(this.getElement());
      });
      this.checkToolState();
    });
    // console.log('this.imageId', this.imageId);
  }

  downloadDoc() {
    const documentCreator = new DocumentCreator();
    const doc = documentCreator.create([
      experiences,
      diagnostics,
      skills,
      achievements,
      this.patientName,
      this.patientId,
      this.toolDataList
    ]);

    Packer.toBlob(doc).then(blob => {
      console.log(blob);
      saveAs(blob, `${this.patientName}.docx`);
    });
  }

  // navbar functions
  openSeries(): void {
    this.isSeriesActive = !this.isSeriesActive;
  }

  openMes(): void {
    this.isMesActive = !this.isMesActive;
  }

  openMore(): void {
    this.isMoreOpen = !this.isMoreOpen;
  }

  openMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }

  // todo create dynamic grid
  createGrid(row: number, col: number): void {
    this.row = row;
    this.col = col;
    this.isHidden = false;
    this.countGrid = [];
    this.isLayoutOpen = false;

    console.log(this.row, this.col);
  }

  layoutDropdown(): void {
    this.isLayoutOpen = !this.isLayoutOpen;
  }

  layoutHover(row: number, col: number): void {
    if (row === 1 && col === 1) {
      this.isHover[1] = true;
      this.isHover[2] = false;
      this.isHover[3] = false;
      this.isHover[4] = false;
      this.isHover[5] = false;
      this.isHover[6] = false;
      this.isHover[7] = false;
      this.isHover[8] = false;
      this.isHover[9] = false;

    }
    if (row === 1 && col === 2) {
      this.isHover[1] = true;
      this.isHover[2] = true;
      this.isHover[3] = false;
      this.isHover[4] = false;
      this.isHover[5] = false;
      this.isHover[6] = false;
      this.isHover[7] = false;
      this.isHover[8] = false;
      this.isHover[9] = false;
    }
    if (row === 1 && col === 3) {
      this.isHover[1] = true;
      this.isHover[2] = true;
      this.isHover[3] = true;
      this.isHover[4] = false;
      this.isHover[5] = false;
      this.isHover[6] = false;
      this.isHover[7] = false;
      this.isHover[8] = false;
      this.isHover[9] = false;
    }
    if (row === 2 && col === 1) {
      this.isHover[1] = true;
      this.isHover[2] = false;
      this.isHover[3] = false;
      this.isHover[4] = true;
      this.isHover[5] = false;
      this.isHover[6] = false;
      this.isHover[7] = false;
      this.isHover[8] = false;
      this.isHover[9] = false;
    }

    if (row === 2 && col === 2) {
      this.isHover[1] = true;
      this.isHover[2] = true;
      this.isHover[3] = false;
      this.isHover[4] = true;
      this.isHover[5] = true;
      this.isHover[6] = false;
      this.isHover[7] = false;
      this.isHover[8] = false;
      this.isHover[9] = false;
    }
    if (row === 2 && col === 3) {
      this.isHover[1] = true;
      this.isHover[2] = true;
      this.isHover[3] = true;
      this.isHover[4] = true;
      this.isHover[5] = true;
      this.isHover[6] = true;
      this.isHover[7] = false;
      this.isHover[8] = false;
      this.isHover[9] = false;
    }
    if (row === 3 && col === 1) {
      this.isHover[1] = true;
      this.isHover[2] = false;
      this.isHover[3] = false;
      this.isHover[4] = true;
      this.isHover[5] = false;
      this.isHover[6] = false;
      this.isHover[7] = true;
      this.isHover[8] = false;
      this.isHover[9] = false;
    }
    if (row === 3 && col === 2) {
      this.isHover[1] = true;
      this.isHover[2] = true;
      this.isHover[3] = false;
      this.isHover[4] = true;
      this.isHover[5] = true;
      this.isHover[6] = false;
      this.isHover[7] = true;
      this.isHover[8] = true;
      this.isHover[9] = false;
    }
    if (row === 3 && col === 3) {
      this.isHover[1] = true;
      this.isHover[2] = true;
      this.isHover[3] = true;
      this.isHover[4] = true;
      this.isHover[5] = true;
      this.isHover[6] = true;
      this.isHover[7] = true;
      this.isHover[8] = true;
      this.isHover[9] = true;
    }
  }

  // cornerstone tools
  stackScrollTool(name): void {
    this.isScrollActive = !this.isScrollActive;
    this.isMenuOpen = false;
    this.activeToolName = name;

    if (this.isScrollActive) {
      const apiTool = cornerstoneTools[`${this.toolName}Tool`];
      cornerstoneTools.addTool(apiTool);
      cornerstoneTools.setToolActive(this.toolName, {mouseButtonMask: 1});
    } else {
      this.toolName = 'StackScroll';
      const apiTool = cornerstoneTools[`${this.toolName}Tool`];
      cornerstoneTools.addTool(apiTool);
      cornerstoneTools.setToolActive(this.toolName, {mouseButtonMask: 1});
    }
  }

  zoomTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    cornerstoneTools.addTool(cornerstoneTools.ZoomTool, {
      // Optional configuration
      configuration: {
        invert: false,
        preventZoomOutsideImage: false,
        minScale: .1,
        maxScale: 20.0,
      }
    });
    cornerstoneTools.setToolActive('Zoom', {mouseButtonMask: 1});
  }

  levelsTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const WwwcTool = cornerstoneTools.WwwcTool;
    const PanTool = cornerstoneTools.PanTool;
    const ZoomTool = cornerstoneTools.ZoomTool;

    cornerstoneTools.addTool(WwwcTool);
    cornerstoneTools.addTool(PanTool);
    cornerstoneTools.addTool(ZoomTool);

    cornerstoneTools.setToolActive('Pan', {mouseButtonMask: 4});
    cornerstoneTools.setToolActive('Zoom', {mouseButtonMask: 2});
    cornerstoneTools.setToolActive('Wwwc', {mouseButtonMask: 1});
  }

  panTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const PanTool = cornerstoneTools.PanTool;

    cornerstoneTools.addTool(PanTool);
    cornerstoneTools.setToolActive('Pan', {mouseButtonMask: 1});
  }

  lengthTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const LengthTool = cornerstoneTools.LengthTool;
    cornerstoneTools.addTool(LengthTool);
    cornerstoneTools.setToolActive('Length', {mouseButtonMask: 1});
  }

  annotateTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const ArrowAnnotateTool = cornerstoneTools.ArrowAnnotateTool;

    cornerstoneTools.addTool(ArrowAnnotateTool);
    cornerstoneTools.setToolActive('ArrowAnnotate', {mouseButtonMask: 1});
  }

  angleTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const AngleTool = cornerstoneTools.AngleTool;

    cornerstoneTools.addTool(AngleTool);
    cornerstoneTools.setToolActive('Angle', {mouseButtonMask: 1});
  }

  getElement(): any {
    return document.getElementById(`dcm-${this.dcmImg}`);
  }

  resetTool(): void {
    this.getElement();
    const viewport = cornerstone.getViewport(this.getElement());
    viewport.hflip = false;
    viewport.vflip = false;
    viewport.rotation = 0;
    cornerstone.setViewport(this.getElement(), viewport);
  }

  magnifyTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const MagnifyTool = cornerstoneTools.MagnifyTool;

    cornerstoneTools.addTool(MagnifyTool);
    cornerstoneTools.setToolActive('Magnify', {mouseButtonMask: 1});
  }

  probeTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const ProbeTool = cornerstoneTools.ProbeTool;

    cornerstoneTools.addTool(ProbeTool);
    cornerstoneTools.setToolActive('Probe', {mouseButtonMask: 1});
  }

  ellipseTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const EllipticalRoiTool = cornerstoneTools.EllipticalRoiTool;

    cornerstoneTools.addTool(EllipticalRoiTool);
    cornerstoneTools.setToolActive('EllipticalRoi', {mouseButtonMask: 1});
  }

  rectangleTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const RectangleRoiTool = cornerstoneTools.RectangleRoiTool;

    cornerstoneTools.addTool(RectangleRoiTool);
    cornerstoneTools.setToolActive('RectangleRoi', {mouseButtonMask: 1});
  }

  invertTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const viewport = cornerstone.getViewport(this.getElement());
    viewport.invert = viewport.invert !== true;
    cornerstone.setViewport(this.getElement(), viewport);
  }

  rotateTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const RotateTool = cornerstoneTools.RotateTool;

    cornerstoneTools.addTool(RotateTool);
    cornerstoneTools.setToolActive('Rotate', {mouseButtonMask: 1});
  }

  hFlipTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const viewport = cornerstone.getViewport(this.getElement());
    viewport.hflip = !viewport.hflip;
    cornerstone.setViewport(this.getElement(), viewport);
  }

  vFlipTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const viewport = cornerstone.getViewport(this.getElement());
    viewport.vflip = !viewport.vflip;
    cornerstone.setViewport(this.getElement(), viewport);
  }

  clearTool(): void {
    cornerstoneTools.globalImageIdSpecificToolStateManager.clear(this.getElement());
    cornerstone.updateImage(this.getElement());
    this.checkToolState();
  }

  bidirectionalTool(name): void {
    this.isMenuOpen = false;
    this.activeToolName = name;

    const BidirectionalTool = cornerstoneTools.BidirectionalTool;

    cornerstoneTools.addTool(BidirectionalTool);
    cornerstoneTools.setToolActive('Bidirectional', {mouseButtonMask: 1});
  }
}
