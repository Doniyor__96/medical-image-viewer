import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../shared/services/auth.service';
import {Router} from '@angular/router';
import {User} from '../shared/interfaces';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  form: FormGroup;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl('', [
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.required
      ])
    });
  }

  submit(): void {
    if (this.form.invalid) {
      return;
    }

    const user: User = {
      username: this.form.value.username,
      password: this.form.value.password
    };

    this.authService.login(user).subscribe(() => {
      // console.log('next', next);
      this.form.reset();
      void this.router.navigate(['/']);
    });
  }

}
